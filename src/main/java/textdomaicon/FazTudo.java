package textdomaicon;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

public class FazTudo {

	public static void main(String[] args) {
		try {
			FileReader arq = new FileReader("/tmp/SBR021.txt");
			BufferedReader lerArq = new BufferedReader(arq);

			String linha = lerArq.readLine(); // lê a primeira linha
			// a variável "linha" recebe o valor "null" quando o processo
			// de repetição atingir o final do arquivo texto
			while (linha != null) {
				if (linha.contains("fct_")) {
					int beginIndex = linha.indexOf("fct_");
					
					String linhaNew = linha.substring(beginIndex, linha.length());
					
					List<Integer> indexes = Arrays.asList(linhaNew.indexOf("("), linhaNew.indexOf(" "), linhaNew.indexOf(";"));
					Integer endIndex = indexes.stream().filter(v -> v > 0).mapToInt(v -> v).min().orElseThrow(NoSuchElementException::new);
							
					System.out.printf("%s\n", linhaNew.substring(0, endIndex));					
				}

				linha = lerArq.readLine(); // lê da segunda até a última linha
			}

			arq.close();
		} catch (IOException e) {
			System.err.printf("Erro na abertura do arquivo: %s.\n", e.getMessage());
		}
	}
}
